module.exports = {
  siteName: "Vivid Hues",
  email: "info@vividhues.online",
  siteTagline: "We do Paint Nights, Art Classes, Private Art Lessons and Painting Commissions!",
  //subTagline: "Paint night in Monsey has never been so easy!"
}
